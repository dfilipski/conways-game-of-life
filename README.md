# Conway's Game of Life in C#

Rules:
1. Any live cell with two or three neighbors survives
2. Any dead cell with three live neighbors becomes a live cell
3. All other live cells  die in the next generation. Similarly all other dead cells stay dead
