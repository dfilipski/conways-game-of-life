﻿using System.Text;

namespace GameOfLife.Library
{
    public class GameOfLifeBoard
    {
        public bool[,] GameBoard { get; set; }

        public GameOfLifeBoard(int rows, int cols)
        {
            GameBoard = new bool[rows, cols];
            AddLife();
            Console.WriteLine($"(2,4): neighbors {CountLivingNeighbhors(2,4)}");
            Console.WriteLine($"(2,4): will live? {WillLive(2,4)}");
        }

        private void AddLife()
        {
            GameBoard[1, 5] = true;
            GameBoard[2, 6] = true;
            GameBoard[3, 4] = true;
            GameBoard[3, 5] = true;
            GameBoard[3, 6] = true;
        }

        public void ProcessGeneration()
        {
            var nextGeneration = new bool[GameBoard.GetLength(0), GameBoard.GetLength(1)];
            for (int i = 0; i < GameBoard.GetLength(0); i++)
                for (int j = 0; j < GameBoard.GetLength(1); j++)
                    nextGeneration[i, j] = WillLive(i, j);
            GameBoard = nextGeneration;
        }

        private bool WillLive(int i, int j)
        {
            int livingNeighbors = CountLivingNeighbhors(i, j);
            bool isAlive = GameBoard[i, j];
            if (isAlive)
            {
                if (livingNeighbors == 2 || livingNeighbors == 3) return true;
            }
            else
            {
                if (livingNeighbors == 3) return true;
            }
            return false;
        }

        private int CountLivingNeighbhors(int row, int col)
        {
            if (row < 0 || row >= GameBoard.GetLength(0)) throw new ArgumentOutOfRangeException(nameof(row));
            if (col < 0 || col >= GameBoard.GetLength(1)) throw new ArgumentOutOfRangeException(nameof(row));

            int count = 0;

            for (int i = row - 1; i <= row + 1; i++)
            {
                if (i < 0 || i >= GameBoard.GetLength(0)) continue; //dont check cells that dont exist
                for (int j = col - 1; j <= col + 1; j++)
                {
                    if (j < 0 || j >= GameBoard.GetLength(1)) continue; //dont check cells that dont exist
                    if (GameBoard[i, j] && (i, j) != (row, col))
                        count++;
                }
            }

            return count;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            for (int i = 0; i < GameBoard.GetLength(0); i++)
            {
                for (int j = 0; j < GameBoard.GetLength(1); j++)
                {
                    if (GameBoard[i, j] == true)
                        sb.Append('*');
                    else
                        sb.Append('.');
                }
                sb.Append('\n');
            }
            return sb.ToString();
        }
    }
}