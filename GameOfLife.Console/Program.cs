﻿using GameOfLife.Library;
using System;

namespace GameOfLife.ConsoleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            var gameOfLife = new GameOfLifeBoard(50, 100);

            while (true)
            {
                Console.Clear();
                gameOfLife.ProcessGeneration();
                Console.WriteLine(gameOfLife);
                System.Threading.Thread.Sleep(500);
            }
        }
    }
}